
// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": preferDefault(require("/Users/filipheden/Documents/Projects/site/.cache/dev-404-page.js")),
  "component---src-pages-404-js": preferDefault(require("/Users/filipheden/Documents/Projects/site/src/pages/404.js")),
  "component---src-pages-index-js": preferDefault(require("/Users/filipheden/Documents/Projects/site/src/pages/index.js")),
  "component---src-pages-resume-js": preferDefault(require("/Users/filipheden/Documents/Projects/site/src/pages/resume.js")),
  "component---src-pages-using-typescript-tsx": preferDefault(require("/Users/filipheden/Documents/Projects/site/src/pages/using-typescript.tsx"))
}

